MobileNations Browser (previously known as AC Browser 1.0)
==========

This application is meant to view all Mobile Nations Forums by pinging a remote server to check the content, then force a re-sync of the data. The application is not able to handle logging in or posting yet, but it will soon. 


# To-Do List
===========
Ability to access forums outside of MobileNations using a plugin/hook.


# Licensing
=============
AC Browser is under a CC0 (Creative Commons) license.

You are permitted to clone the code, modify, and play around with it for personal use. If you do something that you think would be good for the app, feel free to make a pull request; if we like it, it'll be included in the next Marketplace update.

Redistribution of the app (XAB) is not permitted, unless you give credit to Team Panda.

For more information, see: https://creativecommons.org/publicdomain/zero/1.0/legalcode
